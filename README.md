![pipeline status](https://gitlab.com/apiazza134/cft-exam/badges/master/pipeline.svg)

# CFT exam material

- [problems](https://gitlab.com/apiazza134/cft-exam/-/jobs/artifacts/master/raw/problems/cft-problems.pdf?job=compile_pdf)
- [project](https://gitlab.com/apiazza134/cft-exam/-/jobs/artifacts/master/raw/project/cft-project.pdf?job=compile_pdf) (OPE convergence)
